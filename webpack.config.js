const path = require('path');
const argv = require('yargs').argv;
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');

let isDevelopment = argv.mode === 'development';
const isProduction = !isDevelopment;

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  devtool: 'cheap-eval-source-map',
  watch: true,

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [{
          loader: "style-loader"
        }, {
          loader: "css-loader",
          options: {
            minimize: isProduction
          }
        }]
      },
      {
        test: /\.less$/,
        use: [{
          loader: "style-loader"
        }, {
          loader: "css-loader",
          options: {
            minimize: isProduction
          }
        }, {
          loader: "less-loader"
        }]
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "eslint-loader",
        options: {
          "parserOptions": {
            "sourceType": "module"
          },
          "rules": {
            "no-alert": "off",
            "no-console": "off"
          }
        }
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },

  optimization: isProduction ? {
    minimizer: [
      new UglifyJsPlugin({
        uglifyOptions: {
          compress: {
            inline: false,
            warnings: false,
            drop_console: true,
            unsafe: true
          },
        },
      }),
    ]
  }:{},

  optimization: {
    runtimeChunk: {
      name: 'app'
    },
    splitChunks: {
      cacheGroups: {
        default: false,
        commons: {
          test: /src/,
          name: "app",
          chunks: "all",
          minSize: 1
        }
      }
    }
  },

  plugins: [
    new webpack.optimize.ModuleConcatenationPlugin(),
    new CleanWebpackPlugin('dist'),
    new HtmlWebpackPlugin({
      title: 'HomePage',
      template: './index.html',
      chunks:['app']
    }),
    new ExtractTextPlugin('styles.css')
  ],

  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port : 9000,
    compress: true,
    open: true
  }
};


